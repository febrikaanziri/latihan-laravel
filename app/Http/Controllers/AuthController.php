<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.form');
    }

    public function Kirim(Request $request){
        $FirstName = $request['FirstName'];
        $LastName = $request['LastName'];

        return view('halaman.welcome', compact('FirstName','LastName'));
    }
}
